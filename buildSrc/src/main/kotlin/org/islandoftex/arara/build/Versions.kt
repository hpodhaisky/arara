// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.arara.build

object Versions {
    // plugin dependencies
    const val detekt = "1.14.2"
    const val dokka = "1.4.20"
    const val kotlin = "1.4.21"
    const val shadow = "6.1.0"
    const val spotless = "5.8.2"
    const val spotlessChangelog = "2.0.0"
    const val versionsPlugin = "0.36.0"

    // non-plugin dependencies
    const val clikt = "3.1.0"
    const val jna = "5.6.0"
    const val kaml = "0.26.0"
    const val kotest = "4.3.2"
    const val kotlinxSerialization = "1.0.1"
    const val jackson = "2.12.0"
    const val log4j = "2.14.0"
    const val mvel = "2.4.11.Final"
    const val slf4j = "1.7.30"
    const val ztExec = "1.12"
}
